<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>
    <div class="wrapper">
        

        <?php include 'includes/navbar.php' ?>

        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Web Design and Application</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="service">Services</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- About Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/service-1.jpg" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        
                        <div class="about-text text-justify">
                            <p>
                                Webhaus took pride in designing websites ranging from corporate, personal or promotional website that meet your specific needs, reflect your brand and most importantly making you money. Promote your message with the digital language of content, functionality and images. Focusing on usability and simplicity, we create meaningful, captivating sites. We will always strive to build you a complete website by adding our marketing touch to help and bringing in high level of traffic and convert them into sales. We make sure your visitors not only stay longer, but also turn into customers. Under promise, over deliver is our commitment </p>
                            <p>
                                WEBHAUS TECHNOLOGIES offers development of powerful, one-of-a-kind software intended to meet customers’ unique demands. Our skilled specialists know the game of business and deliver successful solutions that accord with all the specified requirements while invariably staying within time and budget limits. We present solutions that satisfy customers’ needs much more precisely than any off-the-shelf product. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About End -->


        <!-- Fact Start -->
        <div class="fact">
            <div class="container-fluid">
                <div class="row counters">
                    <div class="col-md-6 fact-left wow slideInLeft">
                        <div class="row">
                            <div class="col-6">
                                <div class="fact-icon">
                                    <img src="img/web-1.png" alt="Image">
                                </div>
                                <div class="fact-text">
                                    <p>Need to build a web-based applications that connect all your <br> data sources?</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="fact-icon">
                                    <img src="img/web-2.png" alt="Image">
                                </div>
                                <div class="fact-text">
                                    <p>Are you in need of software upgrades but don't want to start from the scratch?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 fact-right wow slideInRight">
                        <div class="row">
                            <div class="col-6">
                                <div class="fact-icon">
                                    <img src="img/web-3.png" alt="Image">
                                </div>
                                <div class="fact-text">
                                    <p>Are you looking for a better way to track your business and data <br>it produces?</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="fact-icon">
                                    <img src="img/web-4.png" alt="Image">
                                </div>
                                <div class="fact-text">
                                    <p>Do you need software that works across multiple devices, from any location?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fact End -->

        <!-- Blog Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    
                    <div class="col-lg-12 col-md-6">
                        
                        <div class="about-text text-center">
  
                            <a class="btn" href="contact">Get In Touch With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog End -->
        <?php include 'includes/footer.php' ?>

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
</body>

<?php include 'includes/script.php' ?>

</html>