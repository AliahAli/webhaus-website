<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>


<body>
    <div class="wrapper">
        


        <?php include 'includes/navbar.php' ?>



        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Our Services</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="">Our Services</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- Service Start -->
        <div class="service">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <a href="web-design">
                            <div class="service-item">
                                <div class="service-img">
                                    <img src="img/service-2.jpg" alt="Image">
                                </div>
                                <div class="service-text ">
                                    <h3 class="text-center">Web Design and Application</h3>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                        <a href="ecommerce">
                            <div class="service-item">
                                <div class="service-img">
                                    <img src="img/service-3.jpg" alt="Image">

                                </div>
                                <div class="service-text ">
                                    <h3 class="text-center">E-Commerce Development</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                        <a href="mobile">
                            <div class="service-item">
                                <div class="service-img">
                                    <img src="img/service-1.jpg" alt="Image">

                                </div>
                                <div class="service-text ">
                                    <h3 class="text-center">Mobile Application</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Service End -->


        <?php include 'includes/footer.php' ?>


        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
</body>

<?php include 'includes/script.php' ?>

</html>