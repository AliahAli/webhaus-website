<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>
    <div class="wrapper">



        <?php include 'includes/navbar.php' ?>

        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>About Us</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="">About Us</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- About Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/about.jpg" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="section-header text-left">
                            <p>Welcome to Webhaus</p>
                            <h2>About Webhaus</h2>
                        </div>
                        <div class="about-text text-justify">
                            <p>
                                Born on the 14th of December 2015 in Malaysia, Webhaus is a web services and mobile solutions provider headquartered in Johor Bahru, Johor. Their primary interests lie in software development, web application and the design of technology. From day one Webhaus has amazed clients and competitors with their rigorous and due diligent approach to tendering for work and completing projects.
                            </p>
                            <p>
                                Webhaus are unchallenged in their commitment to total professionalism, never forgetting to put their clients needs at the forefront of their work it is no surprise that the businesses success is solidly built on long lasting client relationships.
                            </p>
                            <p>
                                Webhaus understand the need for supportive, responsive working relationships, they strive for producing work that speaks to the dynamic nature of the web design world.
                            </p>
                            <p>
                                Our development and design teams are known for their experience with custom projects ranging from alluring website design, to innovative web application development. Each member of our creative team brings a variety of effective ideas and understanding that can only enhance your businesses objectives.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About End -->

        <!-- Fact Start -->
        <div class="fact">
            <div class="container-fluid">
                <div class="row counters">
                    <div class="col-md-6 fact-left wow slideInLeft">
                        <div class="row">
                            <div class="fact-icon">
                                <img src="img/fact-1.png" alt="Image">
                            </div>
                            <div class="col-10">
                                <div class="fact-text">
                                    <h2 class="widget-title">VISION</h2>
                                    <div class="text-widget">
                                        <p>
                                            We aspire to be the leading web services and mobile solutions provider company that would be used by everyone in the nation hence the vision "Grow both horizontally and vertically to occupy a firm position in the competitive <br> nation marketplace".
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 fact-right wow slideInRight">
                        <div class="row">
                            <div class="fact-icon">
                                <img src="img/fact-2.png" alt="Image">
                            </div>
                            <div class="col-10">
                                <div class="fact-text">
                                    <h2 class="widget-title">MISSION</h2>
                                    <div class="text-widget">
                                        <p>
                                            Our mission throughout all these entire years of providing high-quality services to our clients has never changed and it is the same “Fulfill the needs by exceeding clients’ expectations”. Our mission clearly reflects that our team will always be laser-focused on the needs and demands of the clients and we plan to achieve this by formulating unique, proven, and practical web solutions.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fact End -->

        <!-- Portfolio Start -->
        <div class="container">
            <div class="section-header text-center">
                <p>Know our partners</p>
                <h2>Our Partners</h2>
            </div>
            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="row align-items-center">
                    <div class="sidebar-widget wow fadeInUp">
                        <div class="container-fluid">
                            <div class="sidebar ">
                                <div class="sidebar-widget wow fadeInUp text-center ">
                                    <h2 class="widget-title">GOVERNMENT </h2>
                                    <div class="text-widget">
                                        <p>
                                            Government agencies Webhaus have had the privilege of
                                            working with
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="sidebar">
                                        <div class="sidebar-widget wow fadeInUp">
                                            <div class="post-img">
                                                <img src="img/logo-3.png" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="sidebar">
                                        <div class="sidebar-widget wow fadeInUp">
                                            <div class="post-img">
                                                <img src="img/logo-2.png" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="sidebar">
                                        <div class="sidebar-widget wow fadeInUp">
                                            <div class="post-img">
                                                <img src="img/logo-5.png" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="sidebar">
                                        <div class="sidebar-widget wow fadeInUp">
                                            <div class="post-img">
                                                <img src="img/logo-1.png" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="sidebar">
                                        <div class="sidebar-widget wow fadeInUp">
                                            <div class="post-img">
                                                <img src="img/logo-10.png" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="sidebar-widget wow fadeInUp">
                            <div class="container-fluid">
                                <div class="sidebar ">
                                    <div class="sidebar-widget wow fadeInUp text-center ">

                                        <h2 class="widget-title ">NON-GOVERNMENTAL ORGANISATION </h2>
                                        <div>
                                            <p>
                                                Non-Governmental Organisation agencies Webhaus have had the privilege of
                                                working with
                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div class="row counters">
                                    <div class="col-lg-4">
                                        <div class="sidebar">
                                            <div class="sidebar-widget wow fadeInUp">
                                                <div class="post-img">
                                                    <img src="img/logo-11.png" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="sidebar">
                                            <div class="sidebar-widget wow fadeInUp">
                                                <div class="post-img">
                                                    <img src="img/logo-12.png" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="sidebar">
                                            <div class="sidebar-widget wow fadeInUp">
                                                <div class="post-img">
                                                    <img src="img/logo-13.png" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Portfolio End -->

    <?php include 'includes/footer.php' ?>


    <div>
        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    </div>

</body>
<?php include 'includes/script.php' ?>

</html>