<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>
    <div class="wrapper">
        

        <?php include 'includes/navbar.php' ?>

        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Point-Of-Sale System</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="product">Products</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- About Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/product-1.jpg" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <!-- <div class="section-header text-left">
                            <p>Product</p>
                            <h2>Point-Of-Sale System</h2>
                        </div> -->
                        <div class="about-text text-justify">
                            <p>
                                THE BEST, AFFORDABLE AND EASY TO USE
                            </p>
                            <p>
                                WEBHAUS TECHNOLOGIES do more than just process sales and accept payments.Use our features to analyze your sales data and manage your inventory, staff and customers

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About End -->

        <!-- Blog Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    
                    <div class="col-lg-12 col-md-6">
                        
                        <div class="about-text text-center">
  
                            <a class="btn" href="contact">Get In Touch With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog End -->


        <?php include 'includes/footer.php' ?>

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
</body>

<?php include 'includes/script.php' ?>

</html>