<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>
    <div class="wrapper">
        

        <?php include 'includes/navbar.php' ?>

        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Mobile Application</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="service">Services</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- About Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/service-3.jpg" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="about-text text-justify">
                            <p>
                                With experience in the range of development from pure native to cross platform apps, we can help you select the best choice for your project requirement.
                            </p>
                            <p>
                                We believe in guiding our clients all throughout the process of mobile application development for converting the raw app vision into a powerful and high ranking app, to be showcased in the App Store. We will also create an app strategy for your app, support it, improve your idea and provide essential knowledge that allows you to make best business decisions
                            </p>
                            <p>
                                We are constantly driven by our goal of creating innovative and user-friendly apps that too at the most affordable prices. We follow the highest level of business integrity and build a trustworthy relation with our clients. We aim at building products that are high-quality and make us stand out from our competition. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About End -->

         <!-- Blog Start -->
         <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    
                    <div class="col-lg-12 col-md-6">
                        
                        <div class="about-text text-center">
  
                            <a class="btn" href="contact">Get In Touch With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog End -->


        <?php include 'includes/footer.php' ?>

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
</body>

<?php include 'includes/script.php' ?>

</html>