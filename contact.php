<!DOCTYPE html>
<html lang="en">
<?php include 'includes/header.php' ?>

<?php
$details = include 'includes/details.php';
$head_O = $details['head_O'];
$selangor_O = $details['selangor_O'];
$hotline = $details['hotline'];
$email = $details['email'];
$whatsapp = $details['whatsapp'];
?>

<?php

use PHPMailer\PHPMailer\PHPMailer;

$result = "";
if (isset($_POST['submit'])) {

    require 'vendor/autoload.php';
    $setting = require 'includes/email.php';
    $mail = new PHPMailer(true);
    $mail->isSMTP();                     //Set the SMTP server to send through
    $mail->Host       = $setting['host'];                     //Set the SMTP server to send through
    $mail->Port       = $setting['port'];               //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
    $mail->Username   = $setting['username'];                     //SMTP username
    $mail->Password   = $setting['password'];                     //SMTP password

    //Recipients
    $mail->setFrom('ainnuraliah98@gmail.com');
    $mail->addAddress($setting['username']);     //Add a recipient

    //Content
    $mail->isHTML(true);
    $mail->Subject = 'Form Submission:' . $_POST['subject'];
    $mail->Body    = '<h1 align=center>Name :' . $_POST['name'] . '<br>Email: ' . $_POST['email'] . ' <br>Message: ' . $_POST['message'] . '</h1>';

    if ($mail->send()) {
        // Build POST request:
        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_secret = '6Lci-ZEaAAAAAFhPl73dUdJbWvNRDvQkNaQ8MMZa';
        $recaptcha_response = $_POST['recaptcha_response'];

        // Make and decode POST request:
        $get_recaptcha_response = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
        $get_recaptcha_response = json_decode($get_recaptcha_response);

        if ($get_recaptcha_response->success == true && $get_recaptcha_response->score >= 0.5 && $get_recaptcha_response->action == 'submit') {
            $result = '<div class="alert alert-warning" role="alert">
            Verified - email send
            </div>';
            // Verified - send email
        } else {
            $result =  '<div class="alert alert-warning" role="alert">
            Not verified - show form error
            </div>';
            // Not verified - show form error
        }
    } else {
        $result = '<div class="alert alert-warning" role="alert">
        Failed to send 
        </div>';
    }
}
?>

<body>
    <div class="wrapper">

        

        <?php include 'includes/navbar.php' ?>

        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Contact Us</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->

        <!-- Contact Start -->
        <div class="contact wow fadeInUp">
            <div class="container">
                <div class="section-header text-center">
                    <h2>Get In Touch with Us</h2>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact-info">
                            <div class="contact-item">
                                <i class="flaticon-address"></i>
                                <div class="contact-text">
                                    <h2>Head office</h2>
                                    <a href="https://www.google.com/maps/dir/?api=1&destination=Webhaus+Technologies+Sdn+Bhd" target="_blank" ><?= $head_O; ?></a>
                                    <h2 class="mt-3">Selangor office</h2>
                                    <a href="https://goo.gl/maps/xhJeBM1MHq3jXz9S8" target="_blank" ><?= $selangor_O; ?></a>
                                </div>
                            </div>
                            <div class="contact-item">
                                <i class="flaticon-call"></i>
                                <div class="contact-text">
                                    <h2>Phone</h2>
                                    <p><?= $hotline; ?></p>
                                    <p>Click<a href="https://wa.link/4f9ube" target="_blank"> here <i class="fab fa-whatsapp"></i></a> to contact us</p>
                                </div>
                            </div>
                            <div class="contact-item">
                                <i class="flaticon-send-mail"></i>
                                <div class="contact-text">
                                    <h2>Email</h2>
                                    <a href="mailto:<?= $email ?>" target="_blank"><?= $email ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="contact-form">
                            <div id="success"></div>

                            <form method="POST">
                                <h5 class="text-center text-success"><?= $result; ?></h5>

                                <div class="control-group">
                                    <input type="text" class="form-control" name="name" placeholder="Your Name" required="required" data-validation-required-message="Please enter your name" />
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="control-group">
                                    <input type="email" class="form-control" name="email" placeholder="Your Email" required="required" data-validation-required-message="Please enter your email" />
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="control-group">
                                    <input type="text" class="form-control" name="subject" placeholder="Subject" required="required" data-validation-required-message="Please enter a subject" />
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="control-group">
                                    <textarea class="form-control" name="message" placeholder="Message" required="required" data-validation-required-message="Please enter your message"></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>

                                <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">

                                <div>
                                    <input class="btn btn-primary  py-2" type="submit" id="submit" name="submit"></input>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact End -->

        <?php include 'includes/footer.php' ?>

        <a href="#" class="back-to-top" style="right: 280px;"><i class="fa fa-chevron-up"></i></a>
    </div>

</body>

<!-- Contact Javascript File -->
<script src="mail/jqBootstrapValidation.min.js"></script>
<script src="mail/contact.js"></script>

<?php include 'includes/script.php' ?>

<!-- Recaptcha -->
<script src="https://www.google.com/recaptcha/api.js?render=6Lci-ZEaAAAAAFxMZjHtauNnHphfBOLAk5R3f_4y"></script>

<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6Lci-ZEaAAAAAFxMZjHtauNnHphfBOLAk5R3f_4y', {
                action: 'submit'
            })
            .then(function(token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                console.log(recaptchaResponse)
                recaptchaResponse.value = token;
            });
    });
</script>

</html>