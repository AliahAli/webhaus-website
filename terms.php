<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>


<body>
    <div class="wrapper">

        

        <?php include 'includes/navbar.php' ?>

        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Terms & Conditions</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="">Terms & Conditions</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->

        <!-- Terms & Conditions Start-->
        <div class="single">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="single-content wow fadeInUp">
                            <img src="img/terms.jpg" />
                            <h2>Service</h2>
                            <p>
                                WEBHAUS create an account for the Customer and provide to the customer login details for that account. WEBHAUS hereby grants to the customer a licence to use the Hosted Services during the Term. </p>
                            <p>
                                The Customer shall use reasonable endeavours, including reasonable security measures relating to Account access details, to ensure that no unauthorised person may gain access to the Hosted Services. WEBHAUS shall use all reasonable endeavours to maintain the availability of the Hosted Services to the Customer, but does not guarantee 100% availability. </p>
                            <p>
                                For the avoidance of doubt, downtime caused directly or indirectly by any of the following shall not be considered a breach of this Agreement: a Force Majeure Event; a fault or failure of the internet or any public telecommunications network; a fault or failure of the Customer’s computer systems or networks; any breach by the Customer of this Agreement; or scheduled maintenance carried out in accordance with this Agreement.
                            </p>
                            <h3>Refund Policy</h3>
                            <p>
                                Our products are sold ‘as is’. You assume the responsibility for your purchase, and no refunds will be issued. To ensure you are satisfied with your purchase, please read through all the content and material published on our website such as video tutorials, demo, request for consultation and speak to our sales team before making your purchase decision.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Terms & Conditions End-->


        <?php include 'includes/footer.php' ?>


        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
</body>

<?php include 'includes/script.php' ?>

</html>