<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>
    <div class="wrapper">



        <?php include 'includes/navbar.php' ?>

        <!-- Carousel Start -->
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="img/carousel-1.jpg" alt="Carousel Image">
                    <div class="carousel-caption">
                        <!-- <p class="animated fadeInRight">We Are Professional</p> -->
                        <h1 class="animated fadeInLeft">Looking To Develop a Customised Website or App?</h1>
                        <a class="btn animated fadeInUp" href="contact">Talk to Us</a>
                    </div>
                </div>

                <div class="carousel-item">
                    <img src="img/carousel-2.jpg" alt="Carousel Image">
                    <div class="carousel-caption">
                        <!-- <p class="animated fadeInRight">Professional Builder</p> -->
                        <h1 class="animated fadeInLeft">Need Help with Setting Up an<br>E-Commerce Solution<br>and Development?
                        </h1>
                        <a class="btn animated fadeInUp" href="contact">Speak to Us</a>
                    </div>
                </div>

                <div class="carousel-item">
                    <img src="img/carousel-3.jpg" alt="Carousel Image">
                    <div class="carousel-caption">
                        <!-- <p class="animated fadeInRight">We Are Trusted</p> -->
                        <h1 class="animated fadeInLeft">Finding Ways to Improve Productivity in Your Business?</h1>
                        <a class="btn animated fadeInUp" href="contact">Let us Guide you</a>
                    </div>
                </div>
            </div>

            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- Carousel End -->

        <!-- Feature Start-->
        <div class="feature wow fadeInUp" data-wow-delay="0.1s">
            <div class="container-fluid">
                <div class="row gx-0">
                    <div class="col-lg-4 col-md-12">
                        <div class="feature-item">
                            <img src="img/icon-1.png" alt="Image">
                            <div class="feature-text">
                                <h3>Experiences It Develops</h3>
                                <p>
                                    Let our team guide your path for a better solution for <br> your business
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="feature-item">
                            <img src="img/icon-6.png" alt="Image">
                            <div class="feature-text">
                                <h3>Wide Range of Expertise</h3>
                                <p>
                                    Our wide range of knowledge and experiences has allowed up to successful completed a variety of technological based projects
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="feature-item">
                            <img src="img/icon-5.png" alt="Image">
                            <div class="feature-text">
                                <h3>Client Orientated Service</h3>
                                <p>
                                    We ensure all our clients are satisfied with our service and the products produced </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Feature End-->

        <!-- About Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/about.jpg" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="section-header text-left">
                            <p>Welcome to Webhaus</p>
                            <h2>About Webhaus</h2>
                        </div>
                        <div class="about-text text-justify">
                            <p>
                                Born on the 14th of December 2015 in Malaysia, Webhaus is a web services and mobile solutions provider headquartered in Johor Bahru, Johor. Their primary interests lie in software development, web application and the design of technology. From day one Webhaus has amazed clients and competitors
                                with their rigorous and due diligent approach to tendering for work and completing projects.
                            </p>
                            <p>
                                Webhaus are unchallenged in their commitment to total professionalism, never forgetting to put their clients needs at the forefront of their work it is no surprise that the businesses success is solidly built on long lasting client relationships
                            </p>
                            <a class="btn" href="about">Learn More About Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About End -->


        <!-- Fact Start -->
        <div class="fact">
            <div class="container-fluid">
                <div class="row counters">
                    <div class="col-md-6 fact-left wow slideInLeft">
                        <div class="row">
                            <div class="col-6 ">
                                <div class="fact-icon">
                                    <img src="img/icon-7.png" alt="Image">
                                </div>
                                <div class="fact-text">
                                    <p>
                                        Provided Around 500 Technological Solutions
                                    </p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="fact-icon">
                                    <img src="img/icon-3.png" alt="Image">
                                </div>
                                <div class="fact-text">
                                    <!-- <h2 data-toggle="counter-up"></h2> -->
                                    <p>Happy And Satisfied Clients Nationwide</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 fact-right wow slideInRight">
                        <div class="row">
                            <div class="col-6">
                                <div class="fact-icon">
                                    <img src="img/icon-4.png" alt="Image">
                                </div>
                                <div class="fact-text">
                                    <!-- <h2 data-toggle="counter-up"></h2> -->
                                    <p>Experts In Various It Related Sectors</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="fact-icon">
                                    <img src="img/icon-2.png" alt="Image">
                                </div>
                                <div class="fact-text">
                                    <!-- <h2 data-toggle="counter-up"></h2> -->
                                    <p>Numerous Government Related Projects</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fact End -->


        <!-- Service Start -->
        <div class="service">
            <div class="container">
                <div class="section-header text-center">
                    <h2>Our Services & Products</h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="service-item">
                            <div class="service-img">
                                <img src="img/service-1.jpg" alt="Image">
                                <div class="service-overlay">
                                    <p>
                                        Focusing on usability and simplicity, we create meaningful, captivating sites. We make sure your visitors not only stay longer, but also turn into customers.
                                    </p>
                                </div>
                            </div>
                            <div class="service-text">
                                <h3>WEB/MOBILE APPLICATION <br> DESIGN & DEVELOPMENT</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="service-item">
                            <div class="service-img">
                                <img src="img/service-4.jpg" alt="Image">
                                <div class="service-overlay">
                                    <p>We provide a number of additional services that can give your business the applications,protection and <br> resilience required.</p>
                                </div>
                            </div>
                            <div class="service-text">
                                <h3>
                                    SECURITY & FIBRE OPTIC
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="service-item">
                            <div class="service-img">
                                <img src="img/service-2.jpg" alt="Image">
                                <div class="service-overlay">
                                    <p>
                                        The best, affordable and easy to use.Do more than just process sales and accept payments.Use our features to analyze your sales data and manage your inventory, staff and customers
                                    </p>
                                </div>
                            </div>
                            <div class="service-text">
                                <h3>POINT-OF-SALE SYSTEM</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Service End -->

        <!-- Learn Start -->
        <div class="fact">
            <div class="container-fluid">
                <div class="row counters">
                    <div class="col-md-6 fact-left wow slideInLeft">
                        <a href="service">
                            <div class="col-6 col-lg-10">
                                <div class="fact-icon">
                                    <img src="img/learn-1.png" alt="Image">
                                </div>
                                <div class="fact-text">
                                    <a href="service">
                                        Learn More Our Services
                                    </a>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-6 fact-right wow slideInRight">
                        <a href="product">
                            <div class="col-6 col-lg-10 ">
                                <div class="fact-icon">
                                    <img src="img/learn-2.png" alt="Image">
                                </div>
                                <div class="fact-text ">
                                    <a href="product">
                                        Learn More Our Products
                                    </a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Learn End -->

        <!-- FAQs Start -->
        <div id="faq" class="faqs">
            <div class="container">
                <div class="section-header text-center">
                    <p>Frequently Asked Question</p>
                    <h2>You May Ask</h2>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="accordion-1">
                            <div class="card wow fadeInLeft" data-wow-delay="0.1s">
                                <div class="card-header">
                                    <a class="card-link collapsed" data-toggle="collapse" href="#collapseOne">
                                        How much does it cost to develop an app? </a>
                                </div>
                                <div id="collapseOne" class="collapse" data-parent="#accordion-1">
                                    <div class="card-body">
                                        The cost of an app depends on its functionality and complexity.
                                    </div>
                                </div>
                            </div>
                            <div class="card wow fadeInLeft" data-wow-delay="0.2s">
                                <div class="card-header">
                                    <a class="card-link collapsed" data-toggle="collapse" href="#collapseTwo">
                                        Why should I choose Webhaus? </a>
                                </div>
                                <div id="collapseTwo" class="collapse" data-parent="#accordion-1">
                                    <div class="card-body">
                                        Webhaus is a specialist in several different sectors within the digital space. We’ve always guaranteed satisfactory services, products and constantly ensured that we provide the best customer service to our clients. </div>
                                </div>
                            </div>
                            <div class="card wow fadeInLeft" data-wow-delay="0.3s">
                                <div class="card-header">
                                    <a class="card-link collapsed" data-toggle="collapse" href="#collapseThree">
                                        Who are Webhaus’s most notable clients? </a>
                                </div>
                                <div id="collapseThree" class="collapse" data-parent="#accordion-1">
                                    <div class="card-body">
                                        Some of our notable clients are MARA, SME Corp, PUJB, PIJ Holdings, PNS, PHV, PKNS. We are also grateful to have worked with the state government of Negeri Sembilan.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="accordion-2">
                            <div class="card wow fadeInRight" data-wow-delay="0.1s">
                                <div class="card-header">
                                    <a class="card-link collapsed" data-toggle="collapse" href="#collapseFour">
                                        What is the duration to create a website? </a>
                                </div>
                                <div id="collapseFour" class="collapse" data-parent="#accordion-2">
                                    <div class="card-body">
                                        A website can be set up in a week or a month subjecting to our client’s request.
                                    </div>
                                </div>
                            </div>
                            <div class="card wow fadeInRight" data-wow-delay="0.2s">
                                <div class="card-header">
                                    <a class="card-link collapsed" data-toggle="collapse" href="#collapseFive">
                                        When was Webhaus established? </a>
                                </div>
                                <div id="collapseFive" class="collapse" data-parent="#accordion-2">
                                    <div class="card-body">
                                        Webhaus was established in 2015, growing progressively over 6 years by expanding its expertise and client base.
                                    </div>
                                </div>
                            </div>
                            <div class="card wow fadeInRight" data-wow-delay="0.3s">
                                <div class="card-header">
                                    <a class="card-link collapsed" data-toggle="collapse" href="#collapseSix">
                                        Where is Webhaus located? </a>
                                </div>
                                <div id="collapseSix" class="collapse" data-parent="#accordion-2">
                                    <div class="card-body">
                                        We are currently located at Persiaran South Key 1, Johor Bahru </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FAQs End -->

        <!-- Portfolio Start -->
        <div class="portfolio">
            <div class="container">
                <div class="section-header text-center">
                    <p>Our Partners</p>
                    <h2>Notable Clients</h2>
                </div>
                <div class="row portfolio-container">
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.1s">
                        <a href="https://www.mara.gov.my/en/index/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-1.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3> Majlis Amanah Rakyat-Mara</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.4s">
                        <a href="https://www.smecorp.gov.my/index.php/en/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-2.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>SME Corp Malaysia</h3>
                                </div>
                            </div>
                        </a>

                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.6s">
                        <a href="https://www.pujb.my/" target="_blank">

                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-3.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>Perbadanan Usahawan Johor Sdn. Bhd</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.4s">
                        <a href="https://www.pijholdings.com/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-4.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>PIJ Holdings</h3>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.5s">
                        <a href="https://pernas.my/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-5.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>Perbadanan Nasional</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.7s">
                        <a href="https://pernas.my/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-10.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>PERNAS</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.7s">
                        <a href="http://www.pijhalalventures.com/Utama.html" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-6.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>PIJ Halal Ventures</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.8s">
                        <a href="https://www.ns.gov.my/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-7.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>Negeri Sembilan</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.9s">
                        <a href="https://www.facebook.com/phgem/?hc_ref=ART5ZukfhIPNNCjQIldGl_8vwHXkXtrhYhbJ_PqmyE52UTpRS4QbzKktumNR-JSonZA&fref=nf&__tn__=kC-R" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-8.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>Power House Wanita Gemilang</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.9s">
                        <a href="https://www.pkns.gov.my/my/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-9.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>PKNS</h3>
                                </div>
                            </div>
                        </a>

                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- Portfolio End -->

    <?php include 'includes/header.php' ?>
    <?php
    $details = include 'includes/details.php';
    $head_O = $details['head_O'];
    $selangor_O = $details['selangor_O'];
    $hotline = $details['hotline'];
    $email = $details['email'];
    $whatsapp = $details['whatsapp'];
    ?>

    <!-- Footer Start -->
    <div class="footer wow fadeIn" data-wow-delay="0.3s">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="footer-contact">
                        <h2>Office Contact</h2>
                        <p><i class="fa fa-map-marker-alt"></i><a href="https://www.google.com/maps/dir/?api=1&destination=Webhaus+Technologies+Sdn+Bhd" target="_blank"><?= $head_O; ?></a></p>
                        <p><i class="fa fa-map-marker-alt"></i><a href="https://goo.gl/maps/xhJeBM1MHq3jXz9S8" target="_blank"><?= $selangor_O; ?></a></p>
                        <p><i class="fa fa-phone-alt"></i><?= $hotline; ?></p>
                        <p><i class="fa fa-envelope"></i><a href="mailto:<?= $email; ?>" target="_blank"><?= $email; ?></a></p>
                        <div class="footer-social">
                            <a href="https://www.facebook.com/webhausservices/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            <a href="https://www.instagram.com/webhaus_technologies/?hl=en" target="_blank"><i class="fab fa-instagram"></i></a>
                            <a href="https://sg.linkedin.com/company/webhaus-technologies-sdn-bhd" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                            <a href="https://wa.link/4f9ube" target="_blank"><i class="fab fa-whatsapp"></i></a>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="footer-link">
                        <h2>Services Areas</h2>
                        <a href="web-design">Web Application Design & Development</a>
                        <a href="ecommerce">E-Commerce Design & Development</a>
                        <a href="mobile">Mobile Application Design & Development</a>
                        <a href="supply-chain">Supply Chain Management System</a>
                        <a href="point-of-sale">Point-of-Sale System</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="footer-link">
                        <h2>Useful Pages</h2>
                        <a href="about">About Us</a>
                        <a href="contact">Contact Us</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="newsletter">
                        <h2>Follow Us</h2>
                        <p>
                        <div class="fb-page" data-href="https://www.facebook.com/webhausservices/" data-tabs="timeline" data-width="255" data-height="280" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/webhausservices/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/webhausservices/">Webhaus Technologies Sdn Bhd</a></blockquote>
                        </div>
                        </p>
                        <div class="form">
                            <input class="form-control" placeholder="Email here">
                            <button class="btn">Submit</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="container footer-menu">
            <div class="f-menu">
                <a href="terms">Terms of use</a>
                <a href="privacy-policy">Privacy policy</a>
                <a href="">Cookies</a>
                <a href="contact">Help</a>
                <a href="index">FAQs</a>
            </div>
        </div>
        <div class="container copyright">
            <div class="row">
                <div class="col-md-6">
                    <p>&copy; <strong style="color:#0072C6">WEBHAUS TECHNOLOGIES SDN BHD</strong>, All Right Reserved.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer End -->

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
</body>

<?php include 'includes/script.php' ?>

</html>