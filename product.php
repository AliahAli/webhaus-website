<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>
    <div class="wrapper">

       

        <?php include 'includes/navbar.php' ?>

        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Products</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="">Products</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->

        <!-- Service Start -->
        <div class="service">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <a href="point-of-sale">
                            <div class="service-item">
                                <div class="service-img">
                                    <img src="img/product-1.jpg" alt="Image">
                                    <div class="service-overlay">

                                        <p>
                                            THE BEST, AFFORDABLE AND EASY TO USE
                                            <br> Do more than just process sales and accept payments.Use our features to analyze your sales data and manage your inventory, staff and customers
                                        </p>
                                    </div>
                                </div>
                                <div class="service-text ">
                                    <h3 class="text-center">Point-of-sale System</h3>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                        <a href="supply-chain">
                            <div class="service-item">
                                <div class="service-img">
                                    <img src="img/product-2.jpeg" alt="Image">
                                    <div class="service-overlay">

                                        <p>
                                            FAST-MOVING, COST-EFFECTIVE AND ACCURATE
                                            <br>A platform, tool or technology that's used to centralized and track orders, as well as inventory levels and shipments. We offers visibility to both the business and the buyer.
                                        </p>
                                    </div>
                                </div>
                                <div class="service-text ">
                                    <h3 class="text-center">Supply Chain Management System</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Service End -->


        <?php include 'includes/footer.php' ?>

    </div>
    <div>
        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    </div>

</body>
<?php include 'includes/script.php' ?>

</html>