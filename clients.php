<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>


<body>
    <div class="wrapper">
        


        <?php include 'includes/navbar.php' ?>



        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Notable Clients</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="">Notable Clients</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- Portfolio Start -->
        <div class="portfolio">
            <div class="container">
                <div class="row portfolio-container">
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.1s">
                        <a href="https://www.mara.gov.my/en/index/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-1.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3> Majlis Amanah Rakyat-Mara</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.4s">
                        <a href="https://www.smecorp.gov.my/index.php/en/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-2.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>SME Corp Malaysia</h3>
                                </div>
                            </div>
                        </a>

                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.6s">
                        <a href="https://www.pujb.my/" target="_blank">

                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-3.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>Perbadanan Usahawan Johor Sdn. Bhd</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.4s">
                        <a href="https://www.pijholdings.com/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-4.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>PIJ Holdings</h3>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.5s">
                        <a href="https://pernas.my/" target="_blank">
                            <div class="portfolio-warp">
                                <div class="portfolio-img">
                                    <img src="img/logo-5.png" alt="Image">
                                </div>
                                <div class="portfolio-text">
                                    <h3>Perbadanan Nasional</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.7s">
                    <a href="https://pernas.my/" target="_blank">
                        <div class="portfolio-warp">
                            <div class="portfolio-img">
                                <img src="img/logo-10.png" alt="Image">
                            </div>
                            <div class="portfolio-text">
                                <h3>PERNAS</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.7s">
                    <a href="http://www.pijhalalventures.com/Utama.html" target="_blank">
                        <div class="portfolio-warp">
                            <div class="portfolio-img">
                                <img src="img/logo-6.png" alt="Image">
                            </div>
                            <div class="portfolio-text">
                                <h3>PIJ Halal Ventures</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.8s">
                    <a href="https://www.ns.gov.my/" target="_blank">
                        <div class="portfolio-warp">
                            <div class="portfolio-img">
                                <img src="img/logo-7.png" alt="Image">
                            </div>
                            <div class="portfolio-text">
                                <h3>Negeri Sembilan</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.9s">
                    <a href="https://www.facebook.com/phgem/?hc_ref=ART5ZukfhIPNNCjQIldGl_8vwHXkXtrhYhbJ_PqmyE52UTpRS4QbzKktumNR-JSonZA&fref=nf&__tn__=kC-R" target="_blank">
                        <div class="portfolio-warp">
                            <div class="portfolio-img">
                                <img src="img/logo-8.png" alt="Image">
                            </div>
                            <div class="portfolio-text">
                                <h3>Power House Wanita Gemilang</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 portfolio-item first wow fadeInUp" data-wow-delay="0.9s">
                    <a href="https://www.pkns.gov.my/my/" target="_blank">
                        <div class="portfolio-warp">
                            <div class="portfolio-img">
                                <img src="img/logo-9.png" alt="Image">
                            </div>
                            <div class="portfolio-text">
                                <h3>PKNS</h3>
                            </div>
                        </div>
                    </a>

                </div>

                </div>
                
            </div>
        </div>
    </div>
    <!-- Portfolio End -->


        <?php include 'includes/footer.php' ?>


        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>

</body>

<?php include 'includes/script.php' ?>

</html>