<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>
    <div class="wrapper">

        <?php include 'includes/navbar.php' ?>

        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Supply Chain Management System</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="product">Products</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- About Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/product-2.jpeg" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="section-header text-left">
                            <p>Product</p>
                            <h2>Why Do You Need It ?</h2>
                        </div>
                        <div class="about-text text-justify">
                            <p>
                                FAST-MOVING, COST-EFFECTIVE AND ACCURATE
                            </p>
                            <p>
                                A platform, tool or technology that's used to centralized and track orders, as well as inventory levels and shipments. We offers visibility to both the business and the buyer.

                            </p>
                            <p>
                                The beauty of modern-day business is that brands have the ability to reach customers through a variety of channels. Customers can purchase items in store through
                                <br> point-of-sale (POS) software, self-serve through a brand’s direct to-consumer ecommerce store, and
                                even combine online and offline activity with
                                delivery options like buy online, pickup in
                                -store
                                (BOPIS).
                            </p>
                            <p>
                                Managing inventory across different channels is
                                the third largest challenge for supply chain
                                executives. Keeping track of the volume,
                                velocity, and fulfilment of omnichannel orders
                                will bottleneck your business if you haven’t set
                                the right foundations for scalable growth.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About End -->

        <div class="container">
            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="row align-items-center">
                    <div class="sidebar-widget wow fadeInUp">
                        <div class="container-fluid">
                            <div class="sidebar ">
                                <div class="sidebar-widget wow fadeInUp text-center ">
                                    <div class="section-header text-center">
                                        <h2>Why Having Supply Chain Management System Is Important?</h2>
                                    </div>
                                    <div class="text-widget">
                                        <p>
                                            Based on analysis and observations, an OMS helps retailers manage inventory
                                            across the multiple channels they’re selling through. It’s a tool designed to help
                                            the 43% of ecommerce brands that plan to improve their inventory
                                            management process over the next two years.
                                        </p>
                                    </div>
                                </div>
                                <div class="about-text text-justify">
                                    <p>With one, you can:</p>
                                    <ul>
                                        <p><i class="fa fa-check-circle text-primary me-3"></i> Update inventory levels across all sales channel</p>
                                        <p><i class="fa fa-check-circle text-primary me-3"></i> Understand which SKUs sell best through each channel and restock
                                            accordingly</p>
                                        <p><i class="fa fa-check-circle text-primary me-3"></i> View bestselling products</p>
                                    </ul>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog Start -->
        <div class="blog">
            <div class="container">
                <div class="section-header text-center">
                    <h2>What Makes Supply Chain Management System Effective?</h2>
                </div>
                <div class="row blog-page">
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="blog-item">

                            <div class=" blog-title text-center">
                                <h3>Visibility</h3>
                            </div>
                            <div class="blog-text">
                                <p>
                                    View the entire supply chain and isolate events to
                                    anticipate problems and develop more efficient
                                    processes.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp">
                        <div class="blog-item">
                            <div class=" blog-title text-center">
                                <h3>Intelligence</h3>
                            </div>

                            <div class="blog-text">
                                <p>
                                    Tune order management processes to an organization’s
                                    business rules and performance goals. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="blog-item">
                            <div class=" blog-title text-center">
                                <h3>Flexibility
                                </h3>
                            </div>

                            <div class="blog-text">
                                <p>
                                    Break orders or events into unique work items that can be
                                    channeled to the appropriate systems or resources.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="blog-item">

                            <div class=" blog-title text-center">
                                <h3>Real-time inventory</h3>
                            </div>

                            <div class="blog-text">
                                <p>
                                    Get a single view of inventory, see what’s in stock, in
                                    transit and current demand levels-reducing the need to
                                    expedite shipments or maintain excessive <br> safety stock. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp">
                        <div class="blog-item">
                            <div class=" blog-title text-center">
                                <h3>Delivery and service scheduling
                                </h3>
                            </div>

                            <div class="blog-text">
                                <p>
                                    Match delivery commitments to inventory, resources and
                                    skills; allow service requests to be addressed more
                                    efficiently. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="blog-item">
                            <div class=" blog-title text-center">
                                <h3>Customer engagement technologies</h3>
                            </div>

                            <div class="blog-text">
                                <p>
                                    Give customer-facing personnel a view of the customer,
                                    back-end inventory and resources so they can execute
                                    transactions more efficiently. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="blog-item">
                            <div class=" blog-title text-center">
                                <h3>Fulfillment optimization</h3>
                            </div>

                            <div class="blog-text">
                                <p>
                                    Analyze data and recommend options that consider how
                                    and where customers want orders shipped, time-to-delivery and cost </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Blog End -->
    </div>

    <!-- Blog Start -->
    <div class="about wow fadeInUp" data-wow-delay="0.1s">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-12 col-md-6">

                    <div class="about-text text-center">

                        <a class="btn" href="contact">Get In Touch With Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Blog End -->

    <?php include 'includes/footer.php' ?>

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
</body>

<?php include 'includes/script.php' ?>

</html>