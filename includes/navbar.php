<!-- Nav Bar Start -->

<div class="nav-bar">
    <div class="container-fluid ">
        <nav class="navbar navbar-expand-lg bg-dark navbar-dark">
            <a class="logo" href="index">
                <img src="img/webhaus.png" alt="">
            </a>
            <a href="#" class="navbar-brand">MENU</a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-right " id="navbarCollapse">
                <div class="navbar-nav mr-auto">
                    
                </div>
                <a href="index" class="nav-item nav-link ">Home</a>
                    <a href="about" class="nav-item nav-link ">About</a>
                    <div class="nav-item dropdown">
                        <a href="service" class="nav-link dropdown-toggle" data-toggle="dropdown">Services</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="service">All Services</a>
                            <div class="dropdown-divider"></div>
                            <a href="web-design" class="dropdown-item">Web Design and Application</a>
                            <a href="ecommerce" class="dropdown-item">E-Commerce Development</a>
                            <a href="mobile" class="dropdown-item">Mobile Application</a>
                        </div>
                    </div>
                <div class="nav-item dropdown ">
                    <a href="product" class="nav-link dropdown-toggle" data-toggle="dropdown">Products</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="product">All Products</a>
                        <div class="dropdown-divider"></div>
                        <a href="point-of-sale" class="dropdown-item">Point-Of-Sale System</a>
                        <a href="supply-chain" class="dropdown-item">Supply Chain Management System</a>
                    </div>
                </div>
                <a href="clients" class="nav-item nav-link ">Clients</a>
                <a href="contact" class="nav-item nav-link">Contact</a>
            </div>

    </div>

    </nav>
</div>
</div>
<!-- Nav Bar End -->