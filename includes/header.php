<head>
    <?php $config = include 'includes/config.php' ?>
    <?php if ($config['showGa']) { ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $config['gaCode']; ?>"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', '<?php echo $config['gaCode']; ?>');
        </script>
    <?php } ?>

    <meta charset="utf-8">
    <title>Webhaus </title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="Award winning software development company and software consultancy based in Johor Bahru. We design, develop and deliver custom software applications, for desktop and mobile across Malaysia and Singapore." />
    <meta name="author" content="Webhaus" />
    <meta name="revisit-after" content="2 days" />
    <meta name="copyright" content="Webhaus" />
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="index,follow" />
    <meta name="url" content="https://www.webhaus.my/" />

    <!-- Facebook Meta -->
    <meta property="og:url" content="https://www.webhaus.my/" />
    <meta property="og:site_name" content="Webhaus" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Software Development Company | Webhaus Technologies Sdn Bhd | Johor Bahru, Malaysia" />
    <meta property="og:image" content="https://www.webhaus.my/assets/img/Logo_website.png" />
    <meta property="og:description" content="Award winning software development company and software consultancy based in Johor Bahru. We design, develop and deliver custom software applications, for desktop and mobile across Malaysia and Singapore" />

    <meta property="fb:pages" content="327845810688338" />

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- CSS Libraries -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="lib/flaticon/font/flaticon.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    <link href="lib/slick/slick.css" rel="stylesheet">
    <link href="lib/slick/slick-theme.css" rel="stylesheet">
    

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

    <link rel="icon" type="image/png" href="img/favicon-32x32.png">

    <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v12.0" nonce="1HYfIDT7"></script>

</head>