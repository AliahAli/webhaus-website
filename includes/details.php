<?php

return [
  'head_O' => 'B-3-18, Block B, Pusat Komersial Bayu Tasek, Persiaran South Key 1, Kota Southkey, 80150 Johor Bahru, Johor.',
  'selangor_O' => 'A-08-05 Level 8 Block A, Tropicana Avenue, No. 12 Persiaran Tropicana, 47410 Petaling Jaya, Selangor',
  'development_O1' => '<address>Plot E-195, Ground Floor, Industrial Area, Phase 8B, Sector 74, 160071 Sahibzada Ajit Singh Nagar, Punjab, India.</address>',
  'development_O2' => '<address>Office 570, Block E, Wapda Town Phase 1, 66000 Multan, Pakistan.</address>',
  'hotline' => '+603-8408 1770',
  'office' => '+607-336 0065',
  'whatsapp' => '+6014-931 0727',
  'email' => 'sales@webhaus.my',
  'hour' => 'Mon - Fri, 9.00 am - 6.00 pm'
];
