<?php include 'includes/header.php' ?>
<?php
$details = include 'includes/details.php';
$head_O = $details['head_O'];
$selangor_O = $details['selangor_O'];
$hotline = $details['hotline'];
$email = $details['email'];
$whatsapp = $details['whatsapp'];
?>

<!-- Footer Start -->
<div class="footer wow fadeIn" data-wow-delay="0.3s">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="footer-contact">
                    <h2>Office Contact</h2>
                    <p><i class="fa fa-map-marker-alt"></i><a href="https://www.google.com/maps/dir/?api=1&destination=Webhaus+Technologies+Sdn+Bhd" target="_blank"><?= $head_O; ?></a></p>
                    <p><i class="fa fa-map-marker-alt"></i><a href="https://goo.gl/maps/xhJeBM1MHq3jXz9S8" target="_blank"><?= $selangor_O; ?></a></p>
                    <p><i class="fa fa-phone-alt"></i><?= $hotline; ?></p>
                    <p><i class="fa fa-envelope"></i><a href="mailto:<?= $email; ?>" target="_blank"><?= $email; ?></a></p>
                    <div class="footer-social">
                        <a href="https://www.facebook.com/webhausservices/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.instagram.com/webhaus_technologies/?hl=en" target="_blank"><i class="fab fa-instagram"></i></a>
                        <a href="https://sg.linkedin.com/company/webhaus-technologies-sdn-bhd" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                        <a href="https://wa.link/4f9ube" target="_blank"><i class="fab fa-whatsapp"></i></a>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="footer-link">
                    <h2>Services Areas</h2>
                    <a href="web-design">Web Application Design & Development</a>
                    <a href="ecommerce">E-Commerce Design & Development</a>
                    <a href="mobile">Mobile Application Design & Development</a>
                    <a href="supply-chain">Supply Chain Management System</a>
                    <a href="point-of-sale">Point-of-Sale System</a>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="footer-link">
                    <h2>Useful Pages</h2>
                    <a href="about">About Us</a>
                    <a href="contact">Contact Us</a>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="newsletter">
                    <h2>Follow Us</h2>
                    <p>
                    <div class="fb-page" data-href="https://www.facebook.com/webhausservices/" data-tabs="timeline" data-width="255" data-height="280" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/webhausservices/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/webhausservices/">Webhaus Technologies Sdn Bhd</a></blockquote>
                    </div>
                    </p>
                    <div class="form">
                        <input class="form-control" placeholder="Email here">
                        <button class="btn">Submit</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container footer-menu">
        <div class="f-menu">
            <a href="terms">Terms of use</a>
            <a href="privacy-policy">Privacy policy</a>
            <a href="">Cookies</a>
            <a href="contact">Help</a>
            <a href="index">FAQs</a>
        </div>
    </div>
    <div class="container copyright">
        <div class="row">
            <div class="col-md-6">
                <p>&copy; <strong style="color:#0072C6">WEBHAUS TECHNOLOGIES SDN BHD</strong>, All Right Reserved.</p>
            </div>
        </div>
    </div>
</div>

<!-- Footer End -->