<?php
$details = include 'includes/details.php';
$head_O = $details['head_O'];
$selangor_O = $details['selangor_O'];
$hotline = $details['hotline'];
$email = $details['email'];
$whatsapp = $details['whatsapp'];
$hour = $details['hour']
?>

<!-- Top Bar Start -->
<div class="top-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-4 col-md-12">
                <div class="logo">
                    <a href="index">
                        <!-- <h1>Webhaus</h1> -->
                        <img src="img/webhaus.png" alt="Logo">
                    </a>
                </div>
            </div>
            <div class="col-lg-8 col-md-7 d-none d-lg-block">
                <div class="row">
                    <div class="col-4">
                        <div class="top-bar-item">
                            <div class="top-bar-icon">
                                <i class="flaticon-calendar"></i>
                            </div>
                            <div class="top-bar-text">
                                <h3>Opening Hour</h3>
                                <p><?= $hour; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="top-bar-item">
                            <div class="top-bar-icon">
                                <i class="flaticon-call"></i>
                            </div>
                            <div class="top-bar-text">
                                <h3>Call Us</h3>
                                <a href="https://wa.link/4f9ube" target="_blank"><p><?= $whatsapp; ?></p></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="top-bar-item">
                            <div class="top-bar-icon">
                                <i class="flaticon-send-mail"></i>
                            </div>
                            <div class="top-bar-text">
                                <h3>Email Us</h3>
                                <a href="mailto:<?= $email; ?>" target="_blank"><?= $email; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Top Bar End -->