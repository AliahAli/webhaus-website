<!DOCTYPE html>
<html lang="en">

<?php include 'includes/header.php' ?>

<body>
    <div class="wrapper">
        

        <?php include 'includes/navbar.php' ?>

        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>E-Commerce Development</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="service">Services</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->


        <!-- About Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/service-2.jpg" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        
                        <div class="about-text text-justify">
                            <p>
                                We create truly custom ecommerce websites for our clients, treating each project individually and holistically to come up with the best possible solution given your business model, competition, and long term goals. From selling a product or subscription, to taking payments for a service, our team can develop the custom E-Commerce website solution that's perfect for your business.
                            </p>
                            <p>
                                Webhaus E-Commerce Development team is highly specialized in both the technical aspects, as well as the intricacies of E-Commerce development, which enables us to match your shopping cart's technical performance with sales conversions. A successful E-Commerce site, in self-service mode, can control your visitor experience and in the process convert them from visitors to customers.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About End -->

        <!-- Blog Start -->
        <div class="blog">
            <div class="container">
                <div class="section-header text-center">
                    <p>E-Commerce Development</p>
                    <h2>Our E-Commerce Development Features</h2>
                </div>
                <div class="row blog-page">
                    <div class="col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="blog-item">
                            <div class="blog-img">
                                <img src="img/ecommerce-1.jpg" alt="Image">
                            </div>
                            <div class="blog-title">
                                <h3>Drives Sales Across All Devices</h3>
                            </div>
                            <div class="blog-text">
                                <p>
                                    The massive growth of E-Commerce Development is driven by desktop, tablet and smartphone and you'll be able to increase your conversion rate and grow your bottom line across all devices. Advertisers must develop capabilities to understand their customers’ shopping journey across devices, browsers and apps, and ensure a unified customers experience across devices and channels. Investing in E-Commerce Development is therefore worth considering as it can bring a significant uplift in sales </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 wow fadeInUp">
                        <div class="blog-item">
                            <div class="blog-img">
                                <img src="img/ecommerce-2.jpg" alt="Image">
                            </div>
                            <div class="blog-title">
                                <h3>From Homepage to Checkout</h3>
                            </div>
                            <div class="blog-text">
                                <p>
                                    Let us help you simplify your <br> E-Commerce checkout process. Developed using the latest technology and standards, you'll have a blazing fast website that allows customers to go from browsing to checkout in easy way. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="blog-item">
                            <div class="blog-img">
                                <img src="img/ecommerce-3.jpg" alt="Image">
                            </div>
                            <div class="blog-title">
                                <h3>Keep Your Customers Updated</h3>
                            </div>
                            <div class="blog-text">
                                <p>
                                    Keep customers updated with the status of their orders. Customers will receive automatic email notification once their order is verified or shipped. Website administrator will also receive a notification for every sale made, payment received, or when a product inventory is low on stock. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="blog-item">
                            <div class="blog-img">
                                <img src="img/ecommerce-4.jpg" alt="Image">
                            </div>
                            <div class="blog-title">
                                <h3>Easily Manage Your Content & Scale</h3>
                            </div>
                            <div class="blog-text">
                                <p>
                                    Through a well-designed content management system, you can easily update pages, categories, products, articles, posts, banners and more. You have control over the back-end of your store, as well as the look and feel of your site from the customer perspective. You can also easily set up landing pages for promotions or ad campaigns without the help of our Ecommerce Development Team.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog End -->

        <!-- Blog Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    
                    <div class="col-lg-12 col-md-6">
                        
                        <div class="about-text text-center">
  
                            <a class="btn" href="contact">Get In Touch With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog End -->


        <?php include 'includes/footer.php' ?>

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>

</body>

<?php include 'includes/script.php' ?>

</html>